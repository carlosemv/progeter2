#ifndef __TERRAIN_H__
#define __TERRAIN_H__

#include "heights.h"
#include "image.h"

#define MAP_HEIGHT 180
#define MAP_LENGTH 513

void terrainGen();

#endif
