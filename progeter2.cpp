#include <iostream>
#include <cstdlib>
#include <ctime>
//#include <random>

#define N 1025
#define MAX_HEIGHT 90

int uniform(int min, int max)
{
    int range = max - min + 1;
    int buckets = RAND_MAX / range;

    return min + (std::rand() / buckets);
}

template <size_t n, size_t m>
void printArray(int (&array)[n][m])
{
    for (auto i = 0; i < n; ++i)
    {
        for (auto j = 0; j < m; ++j) {
            std::cout << array[i][j] << " ";
            if (array[i][j] < 10)
            	std::cout << " ";
        }
        std::cout << std::endl;
    }
}

void printColor(int height, int maxHeight)
{
	unsigned char r, g, b;
	int pieceSz = (maxHeight*2) / 9;
	if(height < pieceSz-maxHeight) {
		r = 0; g = 0; b = 153; // deep ocean
	}
	else if (height < (2*pieceSz)-maxHeight) {
		r = 0; g = 77; b = 204; // mid ocean
	}
	else if (height < (3*pieceSz)-maxHeight) {
		r = 0; g = 153; b = 255; // shallow ocean
	}
	else if (height < (4*pieceSz)-maxHeight) {
		r = 248; g = 248; b = 153; // sand
	}
	else if (height < (5*pieceSz)-maxHeight) {
		r = 45; g = 191; b = 35; // light vegetation
	}
	else if (height < (6*pieceSz)-maxHeight) {
		r = 6; g = 115; b = 5; // dense vegetation
	}
	else if (height < (7*pieceSz)-maxHeight) {
		r = 175; g = 175; b = 175; // rocky
	}
	else if (height < (8*pieceSz)-maxHeight) {
		r = 111; g = 111; b = 111; // mountain
	}
	else {
		r = 248; g = 248; b = 248; // mountain top
	}

	std::cout << (int)r << " " << (int)g << " " << (int)b << std::endl;
}

template <size_t n>
void DSA(int (&A)[n][n], int step, int v)
{
	if (step <= 1)
		return;
	int hstep = step/2;

	//std::cout << "DIAMONDS!\n";
	// diamonds
	int a, b, c, d;
	for(int i = hstep; i < n-1; i += step)
	{
		for(int j = hstep; j < n-1; j += step)
		{
			//std::cout << i << " " << j << std::endl;
			a = A[i-hstep][j-hstep];
			b = A[i-hstep][j+hstep];
			c = A[i+hstep][j-hstep];
			d = A[i+hstep][j+hstep];

			A[i][j] = ((a+b+c+d)/4) + uniform(-v, v);
		}
	}

	//std::cout << "SQUARES!\n";
	//squares
	int offset = 0;
	for (int i = 0; i < n; i += hstep)
	{
		offset = (offset == 0) ? hstep : 0;
		for (int j = offset; j < n; j += step)
		{
			//std::cout << i << " " << j << std::endl;

			a = (i != 0) ? A[i-hstep][j] : 0;
			b = (j != 0) ? A[i][j-hstep] : 0;
			c = (j != n-1) ? A[i][j+hstep] : 0;
			d = (i != n-1) ? A[i+hstep][j] : 0;

			if (i==0||j==0||i==n-1||j==n-1)
				A[i][j] = ((a+b+c+d)/3) + uniform(-v, v);
			else
				A[i][j] = ((a+b+c+d)/4) + uniform(-v, v);
		}
	}


	DSA(A, hstep, v*0.55);
}

int main()
{
    //std::random_device rd;
    //std::mt19937 gen(rd());
    //std::uniform_int_distribution<> unif_to99(0, 99);
    std::srand(std::time(NULL));
    rand(); rand(); rand();

	int A[N][N];
	A[0][0] = uniform(-MAX_HEIGHT/2, MAX_HEIGHT/2);
	A[0][N-1] = uniform(-MAX_HEIGHT/2, MAX_HEIGHT/2);
	A[N-1][0] = uniform(-MAX_HEIGHT/2, MAX_HEIGHT/2);
	A[N-1][N-1] = uniform(-MAX_HEIGHT/2, MAX_HEIGHT/2);
	DSA(A, N-1, MAX_HEIGHT*0.75);

	std::cout << "P3\n";
	std::cout << N << " " << N << std::endl;
	std::cout << "255\n";
	for (int i = 0; i < N; ++i)
	{
		for (int j = 0; j < N; ++j)
			printColor(A[i][j], MAX_HEIGHT);
	}

	return 0;
}