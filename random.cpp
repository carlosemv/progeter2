#include <iostream>
#include <random>

using namespace std;

random_device rd;
mt19937 gen(rd());

int uniform(int l, int r)
{
	uniform_int_distribution<> unif_to99(l, r);
	return unif_to99(gen);
}

int main ()
{
	for (int i = 0; i < 100; ++i)
	{
		cout << uniform(0, i) << endl;
	}

	return 0;
}